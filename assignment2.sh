#!/bin/sh
echo "Enter FolderName"
read var
mkdir $var
cd $var
for i in 1 2 3 4
do
	touch $var$i
done
cd ..
zip $var.zip $var/*
val=$(echo $var | rev)
mkdir $val
cd $val
unzip ../$var.zip
cd $var
mv * ..
cd ..
rmdir $var
for j in 1 3
do
	chmod 0444 $var$j
done
